package hr.ztel.rovkp;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

import java.io.File;
import java.io.IOException;

import static hr.ztel.rovkp.Recommender1.calculateRecommendations;

public class Recommender2 {

    public static void main(String[] args) throws TasteException, IOException {
        DataModel model = new FileDataModel(new File("/home/mbartolac/Workspaces/java/rovkp_lab3/jester_dataset_2/jester_ratings.dat"), "\t\t");
        UserBasedRecommender recommender = (UserBasedRecommender) new UserBasedRecommenderBuilder().buildRecommender(model);

        //izračunaj i ispiši 10 preporuka za korisnika s ID-jem 22
        calculateRecommendations(recommender);
    }

    // Recommendations for user 22: 99,79,43,74,125,137,122,86,134,116,  -> Recommender1
    // Recommendations for user 22: 125,129,132,127,142,128,138,117,134,121, -> Recommender2

    public static class UserBasedRecommenderBuilder implements RecommenderBuilder {

        @Override
        public Recommender buildRecommender(DataModel model) throws TasteException {
            //inicijaliziraj model učitavanjem podataka o korisnicima

            //inicijaliziraj sličnost korisnika koristeći mjeru log-likelihood
            UserSimilarity similarity = new PearsonCorrelationSimilarity(model);

            //inicijaliziraj slične korisnike kao 5 najsličnijih po mjeri log-likelihood
            UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.5, similarity, model);

            //inicijaliziraj preporučitelja kao preporučitelja temeljenog na suradnji korisnika
            return new GenericUserBasedRecommender(model, neighborhood, similarity);
        }
    }

}
