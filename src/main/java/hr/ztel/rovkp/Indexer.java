package hr.ztel.rovkp;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.regex.Pattern;

public class Indexer {

    private static final Pattern JOKE_RATING_PATTERN = Pattern.compile("[0-9]+:");

    public static void main(String[] args) throws IOException, ParseException {
        Path inputFileItems = Paths.get("jester_dataset_2/jester_items.dat");
        Path outputFile = Paths.get("jester_dataset_2/similarities.dat");

        // 1. Parse input text file with jokes
        Map<Integer, String> jokes = parseJokesFile(inputFileItems);

        // 2. Create and index Lucene documents from parsed jokes
        StandardAnalyzer analyzer = new StandardAnalyzer();
        Directory idx = indexJokes(jokes, analyzer);

        // 3. Calculate similarity between Lucene documents and save it to similarity matrix
        // 4. Normalize similarity matrix
        double[][] similarityMatrix = createSimilarityMatrix(jokes, analyzer, idx);


        // 5. Save Lucene documents similarity to output file
        List<SimilarityPair> pairs = dumpSimilarityMatrix(outputFile, similarityMatrix);

        // Koja šala je najsličnija šali s ID-jem 100? Sala broj 4
        Optional<SimilarityPair> val = pairs.stream()
                .filter(similarityPair -> similarityPair.id1 == 100)
                .max(Comparator.comparingDouble(t -> t.similarity));

        System.out.println("Output size: " + pairs.size());
        System.out.println("Max similarity pair for doc 100: " + val.orElse(null));

        System.out.println(jokes.get(val.get().id1));
        System.out.println(jokes.get(val.get().id2));
    }

    private static List<SimilarityPair> dumpSimilarityMatrix(Path outputFile, double[][] similarityMatrix) throws IOException {
        BufferedWriter fileOutputWriter = Files.newBufferedWriter(outputFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE);
        List<SimilarityPair> pairs = new ArrayList<>();
        for (int i = 0; i < 150; i++) {
            for (int j = 0; j < i; j++) {
                if ((similarityMatrix[i][j] - 0.0) >= 10E-15) {
                    SimilarityPair newPair = new SimilarityPair(i, j, similarityMatrix[i][j]);
                    pairs.add(newPair);
                    fileOutputWriter.write(String.format("%s%n",newPair.toString()));
                }
            }
        }
        fileOutputWriter.close();

        return pairs;
    }

    private static class SimilarityPair {
        private int id1;
        private int id2;
        private double similarity;

        private SimilarityPair(int id1, int id2, double similarity) {
            this.id1 = id1;
            this.id2 = id2;
            this.similarity = similarity;
        }

        @Override
        public String toString() {
            return String.format("%d,%d,%7.5f", id1, id2, similarity);
        }
    }

    private static double[][] createSimilarityMatrix(Map<Integer, String> jokes, StandardAnalyzer analyzer, Directory idx) throws ParseException, IOException {
        double[][] similarityMatrix = new double[jokes.size()][jokes.size()];
        for (Map.Entry<Integer, String> s : jokes.entrySet()) {
            Query query = new QueryParser("joke", analyzer).parse(QueryParser.escape(s.getValue()));
            IndexReader reader = DirectoryReader.open(idx);
            IndexSearcher searcher = new IndexSearcher(reader);

            TopDocs docs = searcher.search(query, jokes.size());
            int rowIndex = s.getKey() - 1;
            ScoreDoc[] hits = docs.scoreDocs;
            double maxRowSimilarity = hits[0].score;
            for (ScoreDoc scoreDoc : hits) {
                int colIndex = scoreDoc.doc;
                similarityMatrix[rowIndex][colIndex] = scoreDoc.score / maxRowSimilarity;
            }
        }

        normalizeMatrix(similarityMatrix);
        return similarityMatrix;
    }

    private static void normalizeMatrix(double[][] similarityMatrix) {
        for (int i = 0; i < similarityMatrix.length; i++) {
            for (int j = i; j < similarityMatrix.length; j++) {
                double avg = (similarityMatrix[i][j] + similarityMatrix[j][i]) / 2;
                similarityMatrix[i][j] = avg;
                similarityMatrix[j][i] = avg;
            }
        }
    }

    private static Directory indexJokes(Map<Integer, String> jokes, StandardAnalyzer analyzer) throws IOException {
        Directory index = new RAMDirectory();

        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexWriter writer = new IndexWriter(index, config);
        for (Map.Entry<Integer, String> joke : jokes.entrySet()) {
            Document doc = new Document();

            Field idField = getField(true, false, IndexOptions.NONE, "ID", joke.getKey().toString());
            Field textField = getField(false, true, IndexOptions.DOCS, "joke", joke.getValue());

            doc.add(idField);
            doc.add(textField);

            writer.addDocument(doc);
        }
        writer.close();

        return index;
    }

    private static Field getField(boolean isStored, boolean isTokenized, IndexOptions opts, String fieldName, String fieldValue) {
        FieldType textFieldType = new FieldType();
        textFieldType.setStored(isStored);
        textFieldType.setTokenized(isTokenized);
        textFieldType.setIndexOptions(opts);
        return new Field(fieldName, fieldValue, textFieldType);
    }

    private static Map<Integer, String> parseJokesFile(Path inputFileItems) throws IOException {
        List<String> items = Files.readAllLines(inputFileItems);
        Map<Integer, String> jokes = new HashMap<>();
        StringBuilder sb = new StringBuilder();
        int jokeId = 0;
        for (String item : items) {
            item = item.trim();
            if (JOKE_RATING_PATTERN.matcher(item).matches()) {
                jokeId = Integer.parseInt(item.substring(0, item.length() - 1));
                continue;
            }

            if (item.length() == 0) {
                jokes.put(jokeId, sb.toString());
                sb.setLength(0);
                continue;
            }

            String jokeFragment = StringEscapeUtils.unescapeXml(item.toLowerCase().replaceAll("<.*?>", ""));
            sb.append(jokeFragment);
        }

        return jokes;
    }
}
