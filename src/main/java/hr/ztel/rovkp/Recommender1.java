package hr.ztel.rovkp;

import org.apache.mahout.cf.taste.common.NoSuchUserException;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.file.FileItemSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.ItemBasedRecommender;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

public class Recommender1 {


    public static void main(String[] args) throws TasteException, IOException {
        DataModel model = new FileDataModel(
                new File("/home/mbartolac/Workspaces/java/rovkp_lab3/jester_dataset_2/jester_ratings.dat"), "\t\t");

        ItemBasedRecommender recommender = (ItemBasedRecommender) new ItemBasedRecommenderBuilder().buildRecommender(model);

        //izračunaj i ispiši 10 preporuka za korisnika s ID-jem 620
        calculateRecommendations(recommender);
    }

    public static void calculateRecommendations(Recommender recommender) throws TasteException, IOException {
        BufferedWriter w = new BufferedWriter(new OutputStreamWriter(System.out));
        for(int i = 1; i <= 100; i++) {
            List<RecommendedItem> recommendations;
            try {
                recommendations = recommender.recommend(i, 10);
            } catch(NoSuchUserException e) {
                continue;
            }
            w.write("Recommendations for user " + i + ": ");
            for (RecommendedItem recommendation : recommendations) {
                long itemID = recommendation.getItemID();
                w.write(itemID + ",");
            }
            w.write("\n");
            w.flush();
        }
    }



    static class ItemBasedRecommenderBuilder implements RecommenderBuilder {
        @Override
        public Recommender buildRecommender(DataModel model) {
            //inicijaliziraj model učitavanjem podataka o korisnicima učitavanjem iz datoteke


            //inicijaliziraj sličnost objekata
            ItemSimilarity similarity = new FileItemSimilarity(
                    new File("/home/mbartolac/Workspaces/java/rovkp_lab3/jester_dataset_2/similarities.dat"));

            //inicijaliziraj preporučitelja kao preporučitelja temeljenog na sličnosti objekata
            return new GenericItemBasedRecommender(model, similarity);
        }

    }
}
