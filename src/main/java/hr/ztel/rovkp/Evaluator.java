package hr.ztel.rovkp;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.RecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.eval.RMSRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.model.DataModel;

import java.io.File;
import java.io.IOException;

public class Evaluator {

    public static void main(String[] args) throws IOException, TasteException {
        DataModel model = new FileDataModel(
                new File("/home/mbartolac/Workspaces/java/rovkp_lab3/jester_dataset_2/jester_ratings.dat"), "\t\t");

//        //evaluiraj procjenu
        RecommenderEvaluator recEvaluator = new RMSRecommenderEvaluator();
        Recommender1.ItemBasedRecommenderBuilder itemBasedRecommenderBuilder = new Recommender1.ItemBasedRecommenderBuilder();
        double score = recEvaluator.evaluate(itemBasedRecommenderBuilder, null, model, 0.3, 0.5);
        System.out.println("ItemBasedRecommenderScore: " + score);

        //evaluiraj procjenu
        RecommenderEvaluator recEvaluator2 = new RMSRecommenderEvaluator();
        Recommender2.UserBasedRecommenderBuilder userBasedRecommenderBuilder = new Recommender2.UserBasedRecommenderBuilder();
        double score2 = recEvaluator2.evaluate(userBasedRecommenderBuilder, null, model, 0.3, 0.5);
        System.out.println("UserBasedRecommenderScore: " + score2);

        //ItemBasedRecommenderScore: 4.701486959104536
        //UserBasedRecommenderScore: 4.91851924163185
    }
}
